let

  # Nounous
  aeltheos_0 = "age1yubikey1qvn7t9hplvnr2w8nsfezfqudz8gq3v8sq99dkdpzmm4a74rng5qgz4v6wzt";
  aeltheos_1 = "age1yubikey1qwmt8heph3jg9sfva3yygphfw0nqed9hs8ndjsfw8yp86kwllearq9fylz2";
  korenstin = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIh26Ejn/syhvReixauY8i85+XD8P9RRJrPQGEyAQ07l klin@nixos";
  lzebulon = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJRSBsRgd/ITK2An7q1VXoeDPbcydR3FkQjHoO+1tAAO lzebulon@archframe";
  pigeonmoelleux = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHa+ptSTNG4mnGUEGSkHTNDzyUGeiMnaWS2nDvJwrYTp ratcornu@skryre";

  nounous = [
    aeltheos_0
    aeltheos_1
    korenstin
    lzebulon
    pigeonmoelleux
  ];

  # Machines

  apprentix = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDCJV6jqQWEYuwi+OJ9r/4TbBN/cK9NvYWNiJhpFzcc7 root@apprentix";
  cephiroth = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOsBGkhiu6l3jeo15cQHMu3dPyL025zXPV2ZH02EDYEt root@nixos";
  jitsi = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB6jVMIZ5y2oXX9HOkw7r5UUjw95MlFaFuu7FnEC0Q8z root@jitsi";
  livre = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEVfKNokHG6ig32hhQxTep+fKFmKahlDClPrX/dP4/gb root@livre";
  neo = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMGfSvxqC2PJYRrxJaivVDujwlwCZ6AwH8hOSA9ktZ1V root@neo";
  redite = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOwfVmR3NjZf6qkDlTSiyo39Up5nSNUVW7jYDWXrY8Xr root@redite";
  thot = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFKNg1b8ft1L55+joXQ/7Dt2QTOdkea8opTEnq4xrhPU root@thot";
  two = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPpaGf8A+XWXBdNrs69RiC0qPbjPHdtkl31OjxrktmF6 root@nixos";

  hosts = {
    inherit
      apprentix
      cephiroth
      jitsi
      livre
      neo
      redite
      thot
      two
      ;
  };

  hostnames = [
    "apprentix"
    "cephiroth"
    "jitsi"
    "livre"
    "neo"
    "redite"
    "thot"
    "two"
  ];

  # Groupes

  all = [
    apprentix
    cephiroth
    jitsi
    livre
    neo
    thot
    two
  ];

  acme = [
    jitsi
    neo
  ];

  # Secrets

  commonSecrets = [ "restic/client_env" ];

  acmeSecrets = [ "acme/env" ];

  # Fonctions utilitaires

  remove = el: list: builtins.filter (x: el != x) list;

  genAttrs =
    paths: groups:
    builtins.foldl' (
      acc: secret: acc // { "secrets/${secret}.age".publicKeys = groups ++ nounous; }
    ) { } paths;

in

(genAttrs commonSecrets (remove apprentix all))
// (genAttrs acmeSecrets acme)
// builtins.foldl' (
  acc: name:
  acc
  // (
    let
      key = hosts.${name};
    in
    genAttrs [
      "restic/${name}/base-repo"
      "restic/${name}/base-password"
    ] [ key ]
  )
) { } (remove "thot" hostnames)
// builtins.mapAttrs (name: value: { publicKeys = value.publicKeys ++ nounous; }) {
  "secrets/common/root.age".publicKeys = remove apprentix all;
  "secrets/apprentix/root.age".publicKeys = [ apprentix ];
  "secrets/neo/database-extra-config.age".publicKeys = [ neo ];
  "secrets/neo/matrix-appservice-irc-password.age".publicKeys = [ neo ];
}
