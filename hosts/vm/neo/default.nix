{ ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
  ];

  boot.loader.grub.devices = [ "/dev/sda" ];

  networking.hostName = "neo";

  system.stateVersion = "23.11";
}
