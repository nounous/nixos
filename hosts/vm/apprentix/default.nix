{ config, lib, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
  ];

  boot.loader.grub.devices = [ "/dev/sda" ];

  networking.hostName = "apprentix";

  security.sudo.extraRules = [
    {
      groups = [ "_user" ];
      commands = [ "ALL" ];
    }
  ];

  age.secrets = {
    root-passwd-hash.file = ../../../secrets/apprentix/root.age;
  };

  users.users.root = {
    hashedPasswordFile = config.age.secrets.root-passwd-hash.path;
  };

  crans.home_nounou.enable = false;

  system.stateVersion = "24.11";
}
