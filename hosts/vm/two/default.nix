{ ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
  ];

  networking.hostName = "two";
  boot.loader.grub.devices = [ "/dev/sda" ];

  system.stateVersion = "23.11";
}
