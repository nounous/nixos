{ config, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
    ../../../modules/services/nginx.nix
    ../../../modules/services/stirling.nix
  ];

  networking.hostName = "livre";
  boot.loader.grub.devices = [ "/dev/sda" ];

  services.nginx.virtualHosts = {
    "pdf.crans.org" = {
      locations."/" = {
        proxyPass = "http://localhost:${toString config.services.stirling-pdf.environment.SERVER_PORT}";
      };
    };
  };

  system.stateVersion = "24.11";
}
