{ ... }:

{
  networking = {
    interfaces = {
      ens18 = {

        ipv4 = {
          addresses = [
            {
              address = "172.16.10.139";
              prefixLength = 24;
            }
          ];
        };

        ipv6 = {
          addresses = [
            {
              address = "fd00::10:0:ff:fe01:3910";
              prefixLength = 64;
            }
          ];
        };

      };

      ens19 = {

        ipv4 = {
          addresses = [
            {
              address = "172.16.3.139";
              prefixLength = 24;
            }
          ];
          routes = [
            {
              address = "0.0.0.0";
              via = "172.16.3.99";
              prefixLength = 0;
            }
          ];
        };

        ipv6 = {
          addresses = [
            {
              address = "2a0c:700:3::ff:fe01:3903";
              prefixLength = 64;
            }
          ];
          routes = [
            {
              address = "::";
              via = "2a0c:700:3::ff:fe00:9903";
              prefixLength = 0;
            }
          ];
        };

      };
    };
  };
}
