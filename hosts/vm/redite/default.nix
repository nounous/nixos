{ ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
    ../../../modules/services/libreddit.nix
  ];

  networking.hostName = "redite";
  boot.loader.grub.devices = [ "/dev/sda" ];

  system.stateVersion = "23.11";
}
