# Machines virtuelles

Voici la liste des machines virtuelles sur NixOS ainsi que leur utilisation (par ordre alphabétique).

## neo

Serveur Matrix (encore non déployé).

## redite

Serveur libreddit, accessible à https://redite.crans.org.

## two

Serveur NixOS de test. Vous pouvez vous en servir comme base pour la configuration d'une nouvelle machine.