{ ... }:

{
  networking = {
    interfaces = {
      ens18 = {

        ipv4 = {
          addresses = [{
            address = "172.16.10.163";
            prefixLength = 24;
          }];
        };

        ipv6 = {
          addresses = [{
            address = "fd00::10:0:ff:fe01:6310";
            prefixLength = 64;
          }];
        };

      };

      ens19 = {

        ipv4 = {
          addresses = [{
            address = "185.230.79.15";
            prefixLength = 26;
          }];
          routes = [{
            address = "0.0.0.0";
            via = "185.230.79.62";
            prefixLength = 0;
          }];
        };

        ipv6 = {
          addresses = [{
            address = "2a0c:700:2::ff:fe01:6302";
            prefixLength = 64;
          }];
          routes = [{
            address = "::";
            via = "2a0c:700:2::ff:fe00:9902";
            prefixLength = 0;
          }];
        };

      };
    };
  };
}
