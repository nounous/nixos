{ ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
    ../../../modules/services/jitsi.nix
    ../../../modules/services/acme.nix
  ];

  networking.hostName = "jitsi";
  boot.loader.grub.devices = [ "/dev/vda" ];

  system.stateVersion = "24.11";
}
