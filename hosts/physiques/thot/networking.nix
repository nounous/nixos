{ ... }:

{
  networking = {

    dhcpcd.enable = false;

    vlans = {
      vlan3 = {
        id = 3;
        interface = "eno1";
      };
      vlan10 = {
        id = 10;
        interface = "eno1";
      };
    };

    interfaces = {
      vlan3 = {
        ipv4 = {
          addresses = [
            {
              address = "172.16.3.14";
              prefixLength = 24;
            }
          ];
        };
        ipv6 = {
          addresses = [
            {
              address = "fd00::10:ae16:2dff:fe76:290c";
              prefixLength = 64;
            }
          ];
        };
      };
      vlan10 = {
        ipv4 = {
          addresses = [
            {
              address = "172.16.10.14";
              prefixLength = 24;
            }
          ];
        };
      };
    };
    defaultGateway = "172.16.3.99";
    nameservers = [ "172.16.10.128" ];
  };
}
