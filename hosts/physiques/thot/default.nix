{ pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./networking.nix

    ../../../modules
    ../../../modules/services/nginx.nix
    ../../../modules/services/restic.nix
  ];

  networking.hostId = "bbdd1133";
  networking.hostName = "thot";

  boot = {
    loader = {
      grub = {
        enable = true;
        zfsSupport = true;
        devices = [
          "/dev/disk/by-id/scsi-3600508b1001ca2198968a17f65ef3963"
          "/dev/disk/by-id/scsi-3600508b1001cd540705e8551011ec02d"
        ];
      };
    };

    zfs = {
      extraPools = [
        "head"
        "backups"
      ];
      forceImportAll = true;
    };
  };

  environment.systemPackages = with pkgs; [
    apacheHttpd
    restic
  ];

  system.stateVersion = "24.05";
}
