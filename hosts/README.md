# Machines

## Machines virtuelles

À l'heure actuelle, seules des machines virtuelles tournent sous NixOS au Crans. Celles-ci se situent dans la baie de superviseurs sous Proxmox Sam-Daniel-Jack. La configuration de toutes les VMs se trouve dans le dossier [`vm`](vm). Vous pouvez trouver plus d'information dans [`vm/README.md`](vm/README.md).

## Comment ajouter une nouvelle machine virtuelle ?

Voici la liste des étapes à suivre pour ajouter une nouvelle machine virtuelle sous NixOS :

- Créer la machine virtuelle avec Proxmox, et l'inscrire dans le LDAP administration en suivant le guide dans [la documentation nounous](https://gitlab.crans.org/nounous/documentation).

- **Avant de démarrer la machine**, écrire la configuration de la machine (sauf le fichier `hardware-configuration.nix` qui est généré automatiquement). Il faut en particulier écrire la configuration réseau et le bootloader. Vous pouvez vous servir de la configuration minimale de la VM de test `two` dans le dossier [`vm/two`](vm/two). La configuration doit être ajoutée à ce dépôt (de préférence sur une branche autre que `main` le temps de la mise en place de la VM).

- Vous pouvez maintenant démarrer la VM et commencer la configuration :

    ```bash
    / $ sudo su
    
    / $ loadkeys fr # ou ce que vous voulez


    # Faire le partitionnement (pour MBR)

    / $ parted /dev/sdX -- mklabel msdos # usuellement sda.

    / $ parted /dev/sdX -- mkpart primary 1MB 100% # Si besoin de swap, mettez moins de 100%

    / $ parted /dev/sdX -- set 1 boot on

    / $ parted /dev/sdX -- mkpart primary linux-swap -8GB 100% # Si besoin de swap

    / $ mkfs.ext4 -L nixos /dev/sdX1

    / $ mkswap -L swap /dev/sdX2 # si besoin de SWAP

    / $ mount /dev/sdX1 /mnt # monter le disque dur sur /mnt, usuellement sda. Attention, ce disque doit correspondre à celui déclaré dans la variable `boot.loader.grub.devices` dans la configuration de la machine.

    # On configure maintenant la configuration réseau
    # Vous n'avez besoin que de configurer le minimum nécessaire pour que la VM ait accès à internet

    / $ systemctl stop dhcpcd.service

    / $ ip addr add 172.16.10.XXX/24 dev [interface] # Faites attention à ce que l'interface réseau soit la bonne

    # Ajouter de la même façon les autres interfaces réseaux

    / $ ip route add default via XXX.XXX.XXX.XXX # Dépend des interfaces réseau configurées dans Proxmox et le LDAP

    / $ echo "nameserver 172.16.10.128" >> /etc/resolv.conf # Ajout du serveur DNS

    / $ ping auro.re # Pour tester la configuration réseau

    / $ nixos-generate-config --root /mnt

    / $ scp /mnt/etc/nixos/hardware-configuration.nix _user@zamok.crans.org:~/hardware-configuration.nix # zamok ou tout autre serveur facile d'accès

    # On copie le fichier hardware-configuration.nix par scp car on ne veut pas que root push sur le git.

    # Faites ce qu'il faut pour que le fichier hardware-configuration soit ajouté au bon endroit dans le dépôt git.

    / $ nix-shell -p git # Ajoute git au shell actuel

    / $ cd /mnt/etc

    /mnt/etc $ rm -rf nixos

    /mnt/etc $ git clone https://gitlab.crans.org/nounous/nixos.git

    /mnt/etc $ nixos-install --flake "/mnt/etc/nixos#<VM>"

    # Mettez un mot de passe quelconque pour le super utilisateur, il ne sera pas remplacé. Pensez donc à le changer après.

    # Vous pouvez maintenant redémarrer la VM et vous ssh dessus en tant que votre _user.
    ```
