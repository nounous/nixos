# Devshells

## Nix

À l'heure actuelle, le devshell `nix` est le seul disponible : celui-ci est dans le fichier [`default.nix`](default.nix). Il contient tout ce qui est nécessaire à la bonne utilisation du langage `nix` pour le développement de cette configuration, à savoir :

- `nil` : un serveur [LSP](https://en.wikipedia.org/wiki/Language_Server_Protocol) pour `nix`

- `nixpkgs-fmt` : un formatter pour `nix`

- `agenix` : un paquet permettant de gérer les secrets pour la configuration. Vous pouvez voir [`../secrets/README.md`](../secrets/README.md) pour plus de détails.

En étant à la racine de la configuration, vous pouvez utiliser ce devshell par la commande `nix develop`.
