{ pkgs, agenix }:

pkgs.mkShell {
  name = "nix";

  packages = with pkgs; [
    agenix.packages.x86_64-linux.default
    age-plugin-yubikey
    nil
    nixpkgs-fmt
    ssh-to-age
  ];
}
