# Modules

Ce dossier contient tous les modules utilisés par les configurations des différentes machines.

## `default.nix`

Le fichier [`default.nix`](default.nix) a deux utilités : il importe par défaut toute la configuration du Crans (voir [la section Crans](#crans)), et importe deux fonctionnalités indispensables pour notre utilisation de nix : les flakes (voir [le `README.md` à la racine du dépôt](../README.md#flakes)) et la commande `nix`. Comprendre en détails cette partie n'est pas nécessaire, il faut juste comprendre qu'il est important de décrire ça.

## Crans

Le dossier [`crans`](crans) contient tous les services/programmes communs à toutes les machines utilisant la configuration usuelle du Crans (les utilisateurices, les `home_nounou`, ...). Vous pouvez voir plus de détails dans [`crans/README.md`](crans/README.md).

## Services

Le dossier [`services`](services) contient tous les services/programmes utilisés par un nombre restreint de machines. On peut y déclarer deux types de configurations : les configurations directement inscrites car seront toujours utilisées de la même façon, et les configurations mettant en place un système d'options et de configuration générée pour avoir plus de granularités. Cette seconde utilisation est plus complexe à mettre en place et nécessite une meilleure compréhension de `nix`.
