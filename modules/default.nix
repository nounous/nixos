{ ... }:

{
  imports = [
    ./crans
  ];

  nix.settings.experimental-features = [
    "flakes"
    "nix-command"
  ];
}
