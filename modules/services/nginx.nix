{ ... }:

{
  services.nginx = {
    enable = true;

    recommendedProxySettings = true;
    recommendedOptimisation = true;

    clientMaxBodySize = "512M";
  };
}
