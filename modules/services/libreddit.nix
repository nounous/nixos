{ ... }:

{
  services.libreddit = {
    openFirewall = true;
    port = 80;
    enable = true;
  };
}
