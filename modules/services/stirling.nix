{ config, ... }:

{
  services.stirling-pdf = {
    enable = true;
    environment = {
      ENDPOINTS_TOREMOVE = "url-to-pdf";
      INSTALL_BOOK_AND_ADVANCED_HTML_OPS = "true";
      SERVER_PORT = 8000;
      SYSTEM_DEFAULTLOCALE = "fr-FR";
    };
  };
}
