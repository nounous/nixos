{ config, ... }:

{
  services.postgresql = {
    enable = true;
    ensureUsers = [
      {
        name = "matrix-synapse";
        ensureDBOwnership = true;
      }
    ];
    ensureDatabases = [ "matrix-synapse" ];
  };

  services.matrix-synapse = {
    enable = false;

    settings = {
      server_name = "crans.org";

      listeners = [
        {
          port = 8008;
          bind_addresses = [
            "127.0.0.1"
            "::1"
          ];
          type = "http";
          tls = false;
          x_forwarded = true;
          resources = [
            {
              name = [
                "client"
                "federation"
              ];
              compress = true;
            }
          ];
        }
      ];
    };
  };
}
