{...}:
{
  services.jitsi-meet = {
    enable = true;
    hostName = "jitsi.crans.org";

    config = {
      # vient de l'ancienne config liveStreamingEnable = true
      liveStreaming.enabled = true;        
    };
  };

  services.jitsi-videobridge = {
    enable = true;

    #xmppConfigs."localhost" = {
    #  port = 5347;
    #};

    openFirewall = true;
  };

  services.jicofo = {
    enable = true;

    config = {
      xmpp = {
        trusted-domains = ["recoder.jitsi.crans.org"];
      };
    };
  };

  services.prometheus.exporters.jitsi = {
    enable = true;
  };
  
}
