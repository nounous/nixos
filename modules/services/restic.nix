{ config, ... }:

{
  services.restic.server = {
    enable = true;

    dataDir = "/backups";
    listenAddress = "localhost:4242";
    privateRepos = true;
  };

  services.nginx.virtualHosts = {
    "${config.networking.hostName}.adm.crans.org" = {
      locations."/" = {
        proxyPass = "http://${config.services.restic.server.listenAddress}";
      };
    };
  };
}
