{ config, ... }:

{
  age.secrets = {
    acme-env.file = ../../secrets/acme/env.age;
  };

  security.acme = {
    acceptTerms = true;

    defaults = {
      email = "root@crans.org";
      dnsPropagationCheck = false;
    };
    certs."crans.org" = {
      domain = "*.crans.org";
      dnsProvider = "rfc2136";
      # Contient le serveur à contacter avec le protocole
      # et le mot de passe
      environmentFile = config.age.secrets.acme-env.path;
    };
  };
}
