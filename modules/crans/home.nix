{
  pkgs,
  lib,
  config,
  ...
}:

let
  cfg = config.crans.home_nounou;
in

{
  options.crans.home_nounou = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = true;
      description = "Monter les home nounous";
    };
  };

  config = lib.mkIf cfg.enable {
    fileSystems.home_nounou = {
      mountPoint = "/home_nounou";
      device = "172.16.10.1:/pool/home";
      fsType = "nfs";
      options = [
        "rw"
        "nosuid"
      ];
    };
  };
}
