# Modules Crans

Ce dossier contient tous les modules commun à la majorité des machines virtuelles du Crans. On y retrouve par exemple les utilisateurices, les `home_nounou`, etc.

## `default.nix`

Le fichier [`default.nix`](default.nix), comme tous les autres du même nom, importe tous les autres fichiers du dossier. De plus, il déclare des programmes utiles à avoir en permanence, tels que `ssh`, `git`, `nvim`, ...

## `home.nix`

Le fichier [`home.nix`](home.nix) monte les `home_nounou` par NFS à partir de `172.16.10.1` (à savoir `tealc`).

## `locale.nix`

Le fichier [`locale.nix`](locale.nix) déclare simplement les locales à utiliser.

## `networking.nix`

Le fichier [`networking.nix`](networking.nix) a moins d'utilité que ce à quoi on pourrait s'attendre : comme chaque machine possède sa propre configuration réseau, les seules choses communes à déclarer sont : la non-utilisation de DHCP, la non-utilisation d'un pare-feu par défault ainsi que l'ajout d'un serveur DNS.

## `ntp.nix`

Le fichier [`ntp.nix`](ntp.nix) active simplement le NTP (Network Time Protocol) en ajoutant le serveur `ntp.adm.crans.org` comme serveur de temps.

## `sops.nix`

Le fichier [`sops.nix`](sops.nix) déclare l'utilisation de `sops` dans la configuration (voir [ce `README.md`](../../secrets/README.md) pour plus de détails) et importe la clef publique SSH de la machine pour pouvoir l'utiliser dans la gestion des secrets.

## `users.nix`

Le fichier [`users.nix`](users.nix) configure les `_users` à partir du LDAP d'administration, et configure les droits pour que les `_nounou` aient les accès `sudo`. Il configure également le user `root` en lui donnant son mot de passe haché à travers un fichier `sops`.
