{ config, lib, ... }:

{
  users = {
    mutableUsers = false;

    ldap = {
      enable = true;
      base = "dc=crans,dc=org";
      server = "ldaps://ldap-adm.adm.crans.org/";
      daemon = {
        enable = true;
        extraConfig = ''
          ldap_version 3
            tls_reqcert allow
            map passwd loginShell /run/current-system/sw/bin/bash
        '';
      };
    };
  };

  security.sudo = {
    enable = true;
    extraConfig = ''
      Defaults	passprompt_override
      Defaults	passprompt="[sudo] mot de passe pour %p sur %h: "
    '';
    extraRules = [
      {
        groups = [ "_user" ];
        runAs = "root:ALL";
        commands = [ "NOPASSWD:/usr/bin/qm list" ];
      }
      {
        groups = [ "_nounou" ];
        commands = [ "ALL" ];
      }
    ];
  };

  age.secrets.root-passwd-hash = {
    file = lib.mkDefault ../../secrets/common/root.age;
  };

  users.users.root = {
    hashedPasswordFile = lib.mkDefault config.age.secrets.root-passwd-hash.path;
  };

  services.openssh.settings.PermitRootLogin = "yes";
}
