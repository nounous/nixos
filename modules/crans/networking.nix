{ lib, ... }:

{
  # Les interfaces ne sont pas déclarées ici : elles sont propres à chaque VM.
  networking = {
    useDHCP = false;
    firewall.enable = lib.mkDefault false;
    nameservers = [ "172.16.10.128" ];
  };
}
