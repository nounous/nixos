{ config, ... }:

{
  age.secrets = {
    restic-base-env.file = ../../secrets/restic/client_env.age;
    restic-base-repo.file = ../../secrets/restic/${config.networking.hostName}/base-repo.age;
    restic-base-password.file = ../../secrets/restic/${config.networking.hostName}/base-password.age;
  };

  services.restic.backups = {
    base = {
      exclude = [
        "/var/cache"
        "/var/lib/lxcfs"
      ];
      initialize = true;
      passwordFile = config.age.secrets.restic-base-password.path;
      repositoryFile = config.age.secrets.restic-base-repo.path;
      environmentFile = config.age.secrets.restic-base-env.path;
      paths = [
        "/etc"
        "/var"
      ];
      timerConfig = {
        OnCalendar = "00:00";
        RandomizedDelaySec = "6h";
      };
      pruneOpts = [
        "--keep-daily 2"
        "--keep-weekly 2"
        "--keep-monthly 2"
        "--keep-yearly 1"
      ];
    };
  };
}
