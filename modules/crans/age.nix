{ ... }:

{
  # This will automatically import host ssh key as age key for secret description.
  age.identityPaths = [
    "/etc/ssh/ssh_host_ed25519_key"
  ];
}
