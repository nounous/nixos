{ ... }:

{
  services.timesyncd = {
    enable = true;
    servers = [ "ntp.adm.crans.org" ];
  };
}
