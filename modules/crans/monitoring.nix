{ config, ... }:
{
  services.prometheus.exporters = {
    node = {
      enable = true;
      port = 9100;

      openFirewall = true;
    };

    nginx = {
      enable = config.services.nginx.enable;
      port = 9117;
      scrapeUri = "http://[::1]:6424/stub_status";
    };
  };
}
