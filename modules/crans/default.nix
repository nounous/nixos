{ pkgs, ... }:

{
  imports = [
    ./age.nix
    ./home.nix
    ./locale.nix
    ./networking.nix
    ./ntp.nix
    ./restic_client.nix
    ./monitoring.nix
    ./users.nix
  ];

  services.qemuGuest.enable = true;
  boot.kernelParams = [ "console=ttyS0,115200" ];

  services.openssh = {
    enable = true;
  };

  nixpkgs.config.allowUnfree = true;

  # Enable some utility programs.
  programs.git.enable = true;
  programs.htop.enable = true;
  programs.neovim.enable = true;
  programs.screen.enable = true;
  programs.tmux.enable = true;
  programs.vim.enable = true;

  environment.systemPackages = with pkgs; [
    nfs-utils
    shelldap
  ];
}
