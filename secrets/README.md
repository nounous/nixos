# Secrets

## Introduction

La gestion des secrets sur NixOS peut se révéler complexe : comme expliquée dans [cette section du `README.md` racine](../README.md#nix-store), aucun secret ne peut apparaître dans le Nix Store. Il faut donc pouvoir charger les secrets dans des fichiers chiffrés qui seront déchiffrés à la reconstruction, sans être pour autant écrits dans le Nix Store. Deux logiciels usuels permettent de faire cela : [`agenix`](https://github.com/ryantm/agenix) et [`sops-nix`](https://github.com/Mic92/sops-nix). Étant donné que `sops` a une utilisation plutôt peu pratique des fichiers de types différents, après l'avoir utilisé pendant un temps le choix a été fait d'utiliser finalement `agenix`. Ce dernier est basé sur l'outil de chiffrement [`age`](https://age-encryption.org/) basé sur les clefs SSH.

## agenix

On ne présentera pas ici tout les détails sur le fonctionnement de `agenix`, vous pouvez allez voir [le `README.md` officiel](https://github.com/ryantm/agenix/) sur GitHub.

### Clefs de chiffrement/déchiffrement

La première chose qu'il faut déterminer pour gérer les serets avec `agenix` est la gestion des clefs de chiffrement et de déchiffrement. On utilise alors des clefs SSH pour les machines et pour les nounous. Plus précisément, on trouve les clefs publiques pour le chiffrement et les clefs privées pour le déchiffrement. La liste des clefs publiques est décrite dans les variables initiales déclarées dans le fichier [`../secrets.nix`](../secrets.nix).

### Secrets

Il faut ensuite chiffrer les secrets que l'on souhaite déclarer. Ces secrets sont stockés dans les différents fichiers avec l'extension `.age` dans ce dossier. Chaque fichier n'a qu'une liste prédéfinie de clefs qui ont le droit de le déchiffrer, ce qui fait que toutes les machines n'ont pas accès à tous les secrets, mais seulement à ceux qui leur sont nécessaire. La liste des clefs utilisable pour chaque fichier se trouve dans la partie `publicKeys` du fichier [`../secrets.nix`](../secrets.nix). Si vous ne comprenez pas parfaitement ce fichier, pas de panique : il est volontairement écrit de telle sorte à ce qu'ajouter une machine et/ou des secrets soit simple au prix d'une plus grande complexité à comprendre parfaitement. Après reconstruction de la configuration, les secrets sont situés dans le dossier `/run/secrets/`, qui n'est pas lisible par les _users, mais seulement par root. Chaque secret possède de plus un user et un groupe propriétaires qui ont les droits de lecture, ce qui peut être utile pour la gestion des secrets pour des services en particulier.

### Utilisation des secrets

Pour utiliser des secrets gérés par `agenix`, deux étapes sont nécessaires :
- déclarer l'existence du fichier contenant le secret par `age.secrets.<name>.file` ;
- utiliser le fichier contenant le secret par `config.age.secrets.<name>.path`.

Vous pouvez voir un exemple d'utilisation de tels secrets dans le fichier [`../modules/crans/users.nix`](../modules/crans/users.nix).

**Attention** : n'utilisez jamais de secret avec de fichier lisant le contenu du fichier pour l'écrire dans la configuration ! Cela fait perdre tout l'intérêt de l'utilisation de `agenix`. Ainsi, il ne faut en aucun cas utiliser les fonctions `lib.readFile`, `lib.fileContents`, ... en combinaison de fichiers contenant des secrets.

### Commandes utiles

Vous devez avoir l'exécutable `agenix` dans votre shell. Vous pouvez pour cela utiliser le [devshell](../devshells/README.md) par défaut avec la commande `nix develop` à la racine du projet.

- Lecture d'un fichier chiffré :
    ```bash
    $ agenix -d [file]
    ```

- Écriture d'un fichier chiffré :

    ```bash
    $ agenix -e [file]
    ```
- Mise à jour d'un fichier après changement des clefs :

    ```bash
    $ agenix -r
    ```

