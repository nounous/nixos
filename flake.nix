{
  description = "Configuration NixOS du Crans";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    flake-parts.url = "github:hercules-ci/flake-parts";

    # Formatter
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Secret management
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-parts,
      agenix,
      ...
    }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [ inputs.treefmt-nix.flakeModule ];

      systems = [ "x86_64-linux" ];

      flake = with nixpkgs.lib; {
        nixosConfigurations =
          let
            baseModules = [ agenix.nixosModules.default ];
          in
          {
            apprentix = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/apprentix ] ++ baseModules;
            };

            jitsi = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/jitsi ] ++ baseModules;
            };

            livre = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/livre ] ++ baseModules;
            };

            neo = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/neo ] ++ baseModules;
            };

            redite = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/redite ] ++ baseModules;
            };

            thot = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/physiques/thot ] ++ baseModules;
            };

            two = nixosSystem {
              specialArgs = inputs;
              modules = [ ./hosts/vm/two ] ++ baseModules;
            };
          };
      };

      perSystem =
        { config, pkgs, ... }:
        {
          treefmt = {
            projectRootFile = "flake.nix";
            programs.nixpkgs-fmt.enable = true;
          };

          devShells = {
            default = pkgs.callPackage ./devshells/default.nix { inherit (inputs) agenix; };
          };
        };
    };
}
